package com.thehun.mickeymall.coupon.dao;

import com.thehun.mickeymall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:39:06
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
