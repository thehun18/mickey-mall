package com.thehun.mickeymall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thehun.common.utils.PageUtils;
import com.thehun.mickeymall.coupon.entity.SeckillSessionEntity;

import java.util.List;
import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:39:06
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SeckillSessionEntity> getLate3DaySession();

}

