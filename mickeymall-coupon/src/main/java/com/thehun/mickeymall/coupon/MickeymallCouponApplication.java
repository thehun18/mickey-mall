package com.thehun.mickeymall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class MickeymallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(MickeymallCouponApplication.class, args);
    }

}
