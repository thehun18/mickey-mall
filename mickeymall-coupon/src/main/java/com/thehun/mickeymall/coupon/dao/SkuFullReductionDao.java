package com.thehun.mickeymall.coupon.dao;

import com.thehun.mickeymall.coupon.entity.SkuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:39:05
 */
@Mapper
public interface SkuFullReductionDao extends BaseMapper<SkuFullReductionEntity> {
	
}
