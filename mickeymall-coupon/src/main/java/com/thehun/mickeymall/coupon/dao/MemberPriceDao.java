package com.thehun.mickeymall.coupon.dao;

import com.thehun.mickeymall.coupon.entity.MemberPriceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品会员价格
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:39:06
 */
@Mapper
public interface MemberPriceDao extends BaseMapper<MemberPriceEntity> {
	
}
