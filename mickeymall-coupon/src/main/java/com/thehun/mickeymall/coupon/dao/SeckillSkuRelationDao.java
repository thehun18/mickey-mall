package com.thehun.mickeymall.coupon.dao;

import com.thehun.mickeymall.coupon.entity.SeckillSkuRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动商品关联
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:39:05
 */
@Mapper
public interface SeckillSkuRelationDao extends BaseMapper<SeckillSkuRelationEntity> {
	
}
