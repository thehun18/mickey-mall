package com.thehun.mickeymall.coupon.dao;

import com.thehun.mickeymall.coupon.entity.HomeSubjectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 专题商品
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:39:06
 */
@Mapper
public interface HomeSubjectSpuDao extends BaseMapper<HomeSubjectSpuEntity> {
	
}
