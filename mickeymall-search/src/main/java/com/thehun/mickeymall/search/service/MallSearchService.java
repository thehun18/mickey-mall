package com.thehun.mickeymall.search.service;

import com.thehun.mickeymall.search.vo.SearchParam;
import com.thehun.mickeymall.search.vo.SearchResult;

public interface MallSearchService {

    /**
     *
     * @param param 检索的所有参数
     * @return 检索结果
     */
    SearchResult search(SearchParam param);
}
