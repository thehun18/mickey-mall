package com.thehun.mickeymall.search.constant;

public class EsConstant {
    // sku
    public static final String PRODUCT_INDEX = "mickeymall_product";

    public static final Integer PRODUCT_PAGE_SIZE = 4;
}
