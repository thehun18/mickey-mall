package com.thehun.mickeymall.search.vo;

import lombok.Data;

import java.util.List;

/**
 * 封装所有可能传入的检索条件
 */
@Data
public class SearchParam {

    private String keyword;
    private Long catalog3Id;// 三级分类id

    private String sort; // 排序字段：销量、价格、热度
    private Integer hasStock; // 0 1
    private String skuPrice; // 1_500 _500 500_
    private List<Long> brandId;
    private List<String> attrs; // attrs=1_aa:bb&attrs=2_aa:bb:cc
    private Integer pageNum = 1;

    private String _queryString; // 原生的查询条件
}
