package com.thehun.mickeymall.search;

import com.alibaba.fastjson.JSON;
import com.thehun.mickeymall.search.config.MickeyMallElasticSearchConfig;
import lombok.Data;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.naming.directory.SearchResult;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MickeymallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @Test
    public void contextLoads() {
        System.out.println(client);
    }

    @Test
    public void indexData() throws IOException {
        // 测试存储数据
        IndexRequest indexRequest = new IndexRequest("users");

        indexRequest.id("1");
//        indexRequest.source("name", "zhangsan", "age", 18);
        User user = new User();
        user.setAge(10);
        user.setName("张三");
        user.setGender("男");
        String jsonString = JSON.toJSONString(user);
        indexRequest.source(jsonString, XContentType.JSON);

        IndexResponse indexResponse = client.index(indexRequest, MickeyMallElasticSearchConfig.COMMON_OPTIONS);

        System.out.println(indexResponse);

    }

    @Data
    class User{
        private String name;
        private String gender;
        private Integer age;
    }

    @Test
    public void searchData() throws IOException {
//        // 创建检索请求
//        SearchRequest searchRequest = new SearchRequest();
//        searchRequest.indices("bank");
//
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//
//        searchRequest.source(sourceBuilder);
//
//        // 执行
//        client.search(searchRequest, MickeyMallElasticSearchConfig.COMMON_OPTIONS);
//
//        // 分析结果
    }

}
