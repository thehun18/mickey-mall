package com.thehun.mickeymall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thehun.common.to.mq.OrderTo;
import com.thehun.common.to.mq.StockLockedTo;
import com.thehun.common.utils.PageUtils;
import com.thehun.mickeymall.ware.entity.WareSkuEntity;
import com.thehun.common.to.SkuHasStockVo;
import com.thehun.mickeymall.ware.vo.WareSkuLockVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:51:02
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void unlockStock(StockLockedTo to);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds);

    Boolean orderLockStock(WareSkuLockVo vo);

    @Transactional
    void unlockStock(OrderTo to);
}

