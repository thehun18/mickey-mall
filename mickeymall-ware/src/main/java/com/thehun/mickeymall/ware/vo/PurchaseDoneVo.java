package com.thehun.mickeymall.ware.vo;

import lombok.Data;

import java.util.List;

@Data
public class PurchaseDoneVo {

    private Long id; // 采购单id
    private List<PurchaseDoneItemVo> items;

}
