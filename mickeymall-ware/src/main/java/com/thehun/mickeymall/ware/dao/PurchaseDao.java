package com.thehun.mickeymall.ware.dao;

import com.thehun.mickeymall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:51:02
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
