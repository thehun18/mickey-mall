package com.thehun.mickeymall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thehun.common.utils.PageUtils;
import com.thehun.mickeymall.ware.entity.PurchaseEntity;
import com.thehun.mickeymall.ware.vo.MergeVo;
import com.thehun.mickeymall.ware.vo.PurchaseDoneVo;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:51:02
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnreceive(Map<String, Object> params);

    void mergePurchase(MergeVo vo);

    void received(List<Long> ids);

    void done(PurchaseDoneVo vo);
}

