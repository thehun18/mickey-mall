/**
  * Copyright 2021 bejson.com 
  */
package com.thehun.mickeymall.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2021-02-14 13:50:30
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Bounds {

    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}