package com.thehun.mickeymall.product.dao;

import com.thehun.mickeymall.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 10:04:00
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {
	
}
