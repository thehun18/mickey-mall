/**
  * Copyright 2021 bejson.com 
  */
package com.thehun.mickeymall.product.vo;

/**
 * Auto-generated: 2021-02-14 13:50:30
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Images {

    private String imgUrl;
    private int defaultImg;
    public void setImgUrl(String imgUrl) {
         this.imgUrl = imgUrl;
     }
     public String getImgUrl() {
         return imgUrl;
     }

    public void setDefaultImg(int defaultImg) {
         this.defaultImg = defaultImg;
     }
     public int getDefaultImg() {
         return defaultImg;
     }

}