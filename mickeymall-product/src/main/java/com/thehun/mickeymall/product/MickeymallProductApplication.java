package com.thehun.mickeymall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession
@MapperScan("com.thehun.mickeymall.product.dao")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.thehun.mickeymall.product.feign")
@SpringBootApplication
public class MickeymallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(MickeymallProductApplication.class, args);
    }

}
