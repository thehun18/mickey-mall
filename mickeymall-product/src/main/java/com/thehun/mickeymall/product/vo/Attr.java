/**
  * Copyright 2021 bejson.com 
  */
package com.thehun.mickeymall.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2021-02-14 13:50:30
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Attr {

    private Long attrId;
    private String attrName;
    private String attrValue;

}