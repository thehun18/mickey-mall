package com.thehun.mickeymall.product.vo;

import com.thehun.mickeymall.product.entity.SkuImagesEntity;
import com.thehun.mickeymall.product.entity.SkuInfoEntity;
import com.thehun.mickeymall.product.entity.SpuInfoDescEntity;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class SkuItemVo {
    SkuInfoEntity info;
    boolean hasStock = true;
    List<SkuImagesEntity> images;
    SpuInfoDescEntity desc;
    List<SkuItemSaleAttrVo> saleAttr;
    List<SpuItemAttrGroupVo> groupAttrs;
    SeckillInfoVo seckillInfoVo;

}
