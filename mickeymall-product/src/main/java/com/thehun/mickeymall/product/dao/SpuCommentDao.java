package com.thehun.mickeymall.product.dao;

import com.thehun.mickeymall.product.entity.SpuCommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 10:04:00
 */
@Mapper
public interface SpuCommentDao extends BaseMapper<SpuCommentEntity> {
	
}
