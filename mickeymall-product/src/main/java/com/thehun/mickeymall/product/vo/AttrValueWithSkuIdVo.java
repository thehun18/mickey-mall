package com.thehun.mickeymall.product.vo;

import lombok.Data;

@Data
public class AttrValueWithSkuIdVo {
    private String attrValue;
    private String skuIds;
}
