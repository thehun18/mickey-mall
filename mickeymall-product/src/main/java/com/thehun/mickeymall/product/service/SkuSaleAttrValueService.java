package com.thehun.mickeymall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thehun.common.utils.PageUtils;
import com.thehun.mickeymall.product.entity.SkuSaleAttrValueEntity;
import com.thehun.mickeymall.product.vo.SkuItemSaleAttrVo;

import java.util.List;
import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 10:03:59
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SkuItemSaleAttrVo> getSaleAttrsBySpuId(Long spuId);

    List<String> getSkuSaleAttrValuesAsStringList(Long skuId);
}

