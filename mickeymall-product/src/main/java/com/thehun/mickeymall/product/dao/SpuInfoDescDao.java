package com.thehun.mickeymall.product.dao;

import com.thehun.mickeymall.product.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 10:03:59
 */
@Mapper
public interface SpuInfoDescDao extends BaseMapper<SpuInfoDescEntity> {
	
}
