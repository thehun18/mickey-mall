package com.thehun.mickeymall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.thehun.mickeymall.product.dao.AttrGroupDao;
import com.thehun.mickeymall.product.dao.SkuSaleAttrValueDao;
import com.thehun.mickeymall.product.entity.BrandEntity;
import com.thehun.mickeymall.product.service.BrandService;
import com.thehun.mickeymall.product.vo.SkuItemSaleAttrVo;
import com.thehun.mickeymall.product.vo.SpuItemAttrGroupVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MickeymallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Autowired
    RedissonClient redissonClient;

    @Autowired
    AttrGroupDao attrGroupDao;

    @Autowired
    SkuSaleAttrValueDao skuSaleAttrValueDao;

    @Test
    public void test() {
//        List<SpuItemAttrGroupVo> group = attrGroupDao.getAttrGroupWithAttrsBySpuId(4L, 225L);
        List<SkuItemSaleAttrVo> group = skuSaleAttrValueDao.getSaleAttrsBySpuId(4L);
        System.out.println(group);
    }

    @Test
    public void testRedisson() {
        System.out.println(redissonClient);
    }

    @Test
    public void contextLoads() {
//        BrandEntity entity = new BrandEntity();
//
//        entity.setDescript("描述信息测试");
//        entity.setName("测试品牌");
//        brandService.save(entity);
//        System.out.println("保存成功");

        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 2L));
        list.forEach((item) -> {
            System.out.println(item);
        });
    }

}
