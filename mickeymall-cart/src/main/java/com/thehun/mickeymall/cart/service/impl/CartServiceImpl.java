package com.thehun.mickeymall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.thehun.common.utils.R;
import com.thehun.mickeymall.cart.feign.ProductFeignService;
import com.thehun.mickeymall.cart.interceptor.CartInterceptor;
import com.thehun.mickeymall.cart.service.CartService;
import com.thehun.mickeymall.cart.vo.Cart;
import com.thehun.mickeymall.cart.vo.CartItem;
import com.thehun.mickeymall.cart.vo.SkuInfoVo;
import com.thehun.mickeymall.cart.vo.UserInfoTo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    ProductFeignService productFeignService;

    @Autowired
    ThreadPoolExecutor executor;

    private final String CART_PREFIX = "cart:";

    @Override
    public CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException {
        // 得到当前用户信息
        BoundHashOperations<String, Object, Object> ops = getCartOps();

        String res = (String) ops.get(skuId.toString());


        if (StringUtils.isEmpty(res)) {

            CartItem cartItem = new CartItem();
            CompletableFuture<Void> getSkuInfoTask = CompletableFuture.runAsync(() -> {
                R skuInfo = productFeignService.getSkuInfo(skuId);
                SkuInfoVo skuInfoData = skuInfo.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                });

                cartItem.setCheck(true);
                cartItem.setCount(num);
                cartItem.setImage(skuInfoData.getSkuDefaultImg());
                cartItem.setTitle(skuInfoData.getSkuTitle());
                cartItem.setPrice(skuInfoData.getPrice());
            }, executor);

            // sku组合信息
            CompletableFuture<Void> getSkuSaleAttrValuesTask = CompletableFuture.runAsync(() -> {
                cartItem.setSkuAttr(productFeignService.getSkuSaleAttrValues(skuId));
            }, executor);

            CompletableFuture.allOf(getSkuInfoTask, getSkuSaleAttrValuesTask).get();
            String jsonString = JSON.toJSONString(cartItem);
            ops.put(skuId.toString(), jsonString);
            return cartItem;
        } else {
            // 有此商品，修改数量
            CartItem cartItem = JSON.parseObject(res, CartItem.class);
            cartItem.setCount(cartItem.getCount() + num);
            // 再放回redis
            ops.put(skuId.toString(), JSON.toJSONString(cartItem));
            return cartItem;
        }
    }

    @Override
    public CartItem getCartItem(Long skuId) {
        BoundHashOperations<String, Object, Object> ops = getCartOps();
        String item = (String) ops.get(skuId.toString());
        CartItem cartItem = JSON.parseObject(item, CartItem.class);
        cartItem.setSkuId(skuId);
        return cartItem;
    }

    @Override
    public Cart getCart() throws ExecutionException, InterruptedException {
        // 区分登录与临时
        Cart cart = new Cart();
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() != null) {
            String userKey = CART_PREFIX + userInfoTo.getUserId();
            BoundHashOperations<String, Object, Object> ops = redisTemplate.boundHashOps(userKey);
            // 合并
            List<CartItem> tempCartItems = getCartItems(CART_PREFIX + userInfoTo.getUserKey());

            if (tempCartItems != null) {
                for (CartItem cartItem : tempCartItems) {
                    addToCart(cartItem.getSkuId(), cartItem.getCount());
                }
                // 1.3 清空临时购物车
                clearCart(CART_PREFIX + userInfoTo.getUserKey());
            }
            // 1.4 获取登录后的购物车数据 [包含合并过来的临时购物车数据]
            List<CartItem> cartItems = getCartItems(userKey);
            cart.setItems(cartItems);
        } else {
            String userKey = CART_PREFIX + userInfoTo.getUserKey();
            List<CartItem> cartItems = getCartItems(userKey);
            cart.setItems(cartItems);
        }
        return cart;
    }

    @Override
    public void clearCart(String cartKey){
        redisTemplate.delete(cartKey);
    }

    @Override
    public void checkItem(Long skuId, Integer check) {
        BoundHashOperations<String, Object, Object> ops = getCartOps();
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCheck(check == 1);
        String s = JSON.toJSONString(cartItem);
        ops.put(skuId.toString(), s);
    }

    @Override
    public void changeItemCount(Long skuId, Integer num) {
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCount(num);
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.put(skuId.toString(), JSON.toJSONString(cartItem));
    }

    @Override
    public void deleteItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.delete(skuId.toString());
    }

    @Override
    public List<CartItem> getUserCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() == null) {
            return null;
        } else {
            String key = CART_PREFIX + userInfoTo.getUserId();
            List<CartItem> cartItems = getCartItems(key);
            // 只返回选中的
            List<CartItem> items = cartItems.stream()
                    .filter(item -> item.getCheck())
                    .map(item -> {
                        R price = productFeignService.getPrice(item.getSkuId());
                        // 更新最新价格
                        item.setPrice(new BigDecimal((String) price.get("data")));
                        return item;
                    })
                    .collect(Collectors.toList());
            return items;
        }
    }

    /**
     * 获取到要添加的购物车
     *
     * @return
     */
    private BoundHashOperations<String, Object, Object> getCartOps() {
        UserInfoTo info = CartInterceptor.threadLocal.get();
        String cartKey = "";
        if (info.getUserId() != null) {
            cartKey = CART_PREFIX + info.getUserId();
        } else {
            cartKey = CART_PREFIX + info.getUserKey();
        }

        BoundHashOperations<String, Object, Object> ops = redisTemplate.boundHashOps(cartKey);
        return ops;
    }

    /**
     * 获取购物车所有项
     */
    private List<CartItem> getCartItems(String cartKey){
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(cartKey);
        Map<Object, Object> values = hashOps.entries();
        if(values != null && values.size() > 0){
            List<CartItem> res = new ArrayList<>();
            Set<Object> keySet = values.keySet();
            List<CartItem> collect = keySet.stream().map(key -> {
                String k = (String) key;
                long skuId = Long.parseLong(k);
                String value = (String) values.get(key);
                CartItem item = JSON.parseObject(value, CartItem.class);
                item.setSkuId(skuId);
                return item;
            }).collect(Collectors.toList());

            return collect;
        }
        return null;
    }
}
