package com.thehun.mickeymall.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class MickeymallThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MickeymallThirdPartyApplication.class, args);
    }

}
