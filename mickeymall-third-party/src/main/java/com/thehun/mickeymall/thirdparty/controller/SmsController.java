package com.thehun.mickeymall.thirdparty.controller;

import com.thehun.common.utils.R;
import com.thehun.mickeymall.thirdparty.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sms")
public class SmsController {

    @Autowired
    SmsComponent smsComponent;

    @GetMapping("/sendcode")
    public R sendCode(@RequestParam("phone") String phoneNum) {
        smsComponent.sendSmsCode(phoneNum);
        return R.ok();
    }
}
