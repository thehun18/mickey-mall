package com.thehun.mickeymall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thehun.common.utils.PageUtils;
import com.thehun.mickeymall.order.entity.OrderItemEntity;

import java.util.Map;

/**
 * 订单项信息
 *
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:50:11
 */
public interface OrderItemService extends IService<OrderItemEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

