package com.thehun.mickeymall.order.dao;

import com.thehun.mickeymall.order.entity.PaymentInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 支付信息表
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:50:10
 */
@Mapper
public interface PaymentInfoDao extends BaseMapper<PaymentInfoEntity> {
	
}
