package com.thehun.mickeymall.order.feign;

import com.thehun.mickeymall.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("mickeymall-cart")
public interface CartFeignService {

    @GetMapping("/currentUserCartItem")
    public List<OrderItemVo> currentUserCartItem();
}
