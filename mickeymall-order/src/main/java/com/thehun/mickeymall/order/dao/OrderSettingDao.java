package com.thehun.mickeymall.order.dao;

import com.thehun.mickeymall.order.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:50:10
 */
@Mapper
public interface OrderSettingDao extends BaseMapper<OrderSettingEntity> {
	
}
