package com.thehun.mickeymall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thehun.common.utils.PageUtils;
import com.thehun.mickeymall.order.entity.OrderReturnApplyEntity;

import java.util.Map;

/**
 * 订单退货申请
 *
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:50:10
 */
public interface OrderReturnApplyService extends IService<OrderReturnApplyEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

