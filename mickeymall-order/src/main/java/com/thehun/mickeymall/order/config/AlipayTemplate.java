package com.thehun.mickeymall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.thehun.mickeymall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2021000117613217";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCEWHiaewgevIayaFirb9a+jDbm/rpsoBdDD2yC1f7/TFy5in5Y8fovsFBwMTqLnZakRndbNVnsngGaJ303fKvtwlB/OSpx5IzhZa6vujVm1A/Uw6RvQ7QFFhxSUPuWcWXoiTOXJKDovRL74EAG0GjxHXk1G5betOVa8glmbfvZZysE7dB3XjNMAoryn7vBJg3Oww5NW6M2wCwyPn1b+F/nncWw+boVtk+h2At1PCpH6CFOsV2SqHLH7tuK1FKOnrdkgtM7XUnh+HMaEl1OTDufXUTuL+Hb1I+SsrB+16l5uGGG93yOoYWqDKQ30dYegId9Vrg99RHGP4xSl9nrWEOzAgMBAAECggEABuLbk54nNAI3N/057+Gn2M5O5QIIDTLfQvwAd2ZPGFWO1ZChNluQ2v5fkRbepvkSJ4YLG9HQ1gD6sFyxpTt1bq+XpKcC0F5F/kxeJIKZ2LncAQQVAD0TltgprRbNoGxSEyFgb9LWFIYSAM0QR8p8S19vORYwaXIcJPlHB+9dFVi3P8juRLjVu0Vs+zLo0SKQM7WxZsrKIcKYsfcrzHo4GDYzdc0xYV5MpmeS0DurWAII72fMmEHYKLU+BTU7G0ggrRP/gWk06l3lU3LZYz8zdK5y9alvmscIO1syvpKWDP8qw1+uNqGVoSE3y6knhjB01YWJqqVuZzwfWqvp/eztcQKBgQDmJNjzbgJYCye9GpSmKgHT6SrQOLKWt7BFymX3YS7xJ23KbWUi5+QwgFzAbwcHoO3uMXxsiBQ3ZUkxRnpZ0Q5ngJ54ytRfXmCtn4FFgtFsjW+eFcFS4UrLo4KVwVTU8AkGB1iw+ibSQqQAh50bTIDJHliaIeyNTWdOI6GM4lXqJwKBgQCTNtpNX09cOKjvuuxNl30DORWIEQrgAvx/NnUk3HS0/KygubJhiNVsZ0hzun9W7XFYZHioXbowFTdnHh7KToNAoocRgz/rmNpPNmMV1fIfaEUQY+Ntet2fHobhs6bRkPNkUQqhqmp3JDrGdNOlZguHRNbXE4rqhZFUWHUgANUNlQKBgBG1ligtN+r0eMtZZ0TeCKP41j2Nj1h/rAXCzFPb25Pc99rSziyAcZO13O1F3eGdZZGlpzBmzD9zAM3U4YdVyej3AbrDyNHJ3mCd3IGvzlOvEn60p9qr0WiYHW1Ooit8iQU4vX9o0GHvlNUhmlpGacaQGjtKDvzUjvV5Um4X4IqTAoGBAI6wIhPQh7lZadVDFnt2k8Ml90q1ShcqjdY30H9z9uLd7eyzUMTcvhCeY0lYwMheyPfmGlUG08p7JgIa3bk1NF7XqZc94hzXQPJ6QrwNbOsoQ5jJWMzqxBjQm+f0G4MQvfjD3EwqQ5tLUHS/E81DBXWNMbexx3ndEKnyO0RTyi2pAoGBAOT1xhaVCfLD/1fTWXNckDwlu4da/+Okzw4Lkw7v1tAWB3xhQoEOe98MU0A1JrU9eL3z84ZRrMnSU2PKch5UzsumKTHNb7ElZRY9KgLlTj/dBP4kaTwpqR0+LGtLNTM6IiIbbd/lh0IQFsRkcuAL6ZsZKFqL7Ron1qlgS5aekX2D";	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjedTc7aQdOm/hEaZG9fnckhuJvVQodrNIxBf+McEeOdmFCv6VbJVCSzwqonizOO2C+xVuFNi8rBnvm5CMnVMuNzxmJy9JJZMI5gbSZaPUrsmpvd8LXFLvjuCWnv2NBMrf6VbtOedASWHrI4dV2orAsqermLSmep2f9VKC4yR+QDi7rXz9BMVGInIzMwSDoc3AkMiubYWbKeAh1TE/NoFWr5njlrKYfEz5mvfmXDABIQtP7wg0Rx9+8yaWPs8ExTzE3UBTtAU5mmwfxrXLERTFL9d3arPqEagKxMI4C05+yvDz64hyVVG9jnFiruUuFinO6BP1huZTCg1eSk2MjR6lwIDAQAB";	// 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url = "http://member.mickeymall.com/memberOrder.html";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url = "http://member.mickeymall.com/memberOrder.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    // 自动关单时间
    private String timeout = "15m";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        // 30分钟内不付款就会自动关单
        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"timeout_express\":\"" + timeout + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        return result;
    }
}
