package com.thehun.mickeymall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thehun.common.to.mq.SecKillOrderTo;
import com.thehun.common.utils.PageUtils;
import com.thehun.mickeymall.order.entity.OrderEntity;
import com.thehun.mickeymall.order.vo.*;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:50:11
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException;

    SubmitOrderResponseVo submitOrder(OrderSubmitVo submitVo);

    void closeOrder(OrderEntity entity);

    PageUtils queryPageWithItem(Map<String, Object> params);

    OrderEntity getOrderByOrderSn(String orderSn);

    PayVo getOrderPay(String orderSn);

    String handlePayResult(PayAsyncVo vo);

    void createSecKillOrder(SecKillOrderTo secKillOrderTo);
}

