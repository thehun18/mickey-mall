package com.thehun.mickeymall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thehun.common.utils.PageUtils;
import com.thehun.mickeymall.order.entity.OrderReturnReasonEntity;

import java.util.Map;

/**
 * 退货原因
 *
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:50:10
 */
public interface OrderReturnReasonService extends IService<OrderReturnReasonEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

