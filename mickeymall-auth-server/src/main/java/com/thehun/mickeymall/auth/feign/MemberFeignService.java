package com.thehun.mickeymall.auth.feign;

import com.thehun.common.utils.R;
import com.thehun.mickeymall.auth.vo.SocialUser;
import com.thehun.mickeymall.auth.vo.UserLoginVo;
import com.thehun.mickeymall.auth.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("mickeymall-member")
public interface MemberFeignService {

    @PostMapping("/member/member/regist")
    R regist(@RequestBody UserRegistVo vo);

    @PostMapping("/member/member/login")
    R login(@RequestBody UserLoginVo vo);

    @PostMapping("/member/member/oauth2/login")
    public R oauthlogin(@RequestBody SocialUser vo) throws Exception;
}
