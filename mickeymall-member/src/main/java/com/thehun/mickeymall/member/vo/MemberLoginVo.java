package com.thehun.mickeymall.member.vo;

import lombok.Data;

@Data
public class MemberLoginVo {
    private String loginacct;
    private String password;
}
