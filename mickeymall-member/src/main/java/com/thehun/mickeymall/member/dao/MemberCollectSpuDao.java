package com.thehun.mickeymall.member.dao;

import com.thehun.mickeymall.member.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:48:26
 */
@Mapper
public interface MemberCollectSpuDao extends BaseMapper<MemberCollectSpuEntity> {
	
}
