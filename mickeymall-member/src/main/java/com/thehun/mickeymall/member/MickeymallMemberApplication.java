package com.thehun.mickeymall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.thehun.mickeymall.member.feign")
@SpringBootApplication
public class MickeymallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(MickeymallMemberApplication.class, args);
    }

}
