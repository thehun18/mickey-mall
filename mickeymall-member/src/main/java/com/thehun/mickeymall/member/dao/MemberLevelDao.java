package com.thehun.mickeymall.member.dao;

import com.thehun.mickeymall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:48:26
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {

    MemberLevelEntity getDefaultLevel();
}
