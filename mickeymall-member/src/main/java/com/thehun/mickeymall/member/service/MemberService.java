package com.thehun.mickeymall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thehun.common.utils.PageUtils;
import com.thehun.mickeymall.member.entity.MemberEntity;
import com.thehun.mickeymall.member.exception.PhoneExistException;
import com.thehun.mickeymall.member.exception.UsernameExistException;
import com.thehun.mickeymall.member.vo.MemberLoginVo;
import com.thehun.mickeymall.member.vo.MemberRegistVo;
import com.thehun.mickeymall.member.vo.SocialUser;

import java.util.Map;

/**
 * 会员
 *
 * @author yanglin
 * @email hustyanglin@qq.com
 * @date 2021-02-11 21:48:26
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberRegistVo vo);

    void checkUsernameUnique(String username) throws UsernameExistException;

    void checkPhoneUnique(String phone) throws PhoneExistException;

    MemberEntity login(MemberLoginVo vo);

    MemberEntity login(SocialUser vo);
}

